import os
from dotenv import load_dotenv
from flask import Flask, request
from flask_login import LoginManager, UserMixin, login_required
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

app1 = Flask(__name__)
app1.secret_key = 'krovostok'
app1.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:Lfirbyp1@db:5432/GPB_test'
app1.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app1)
ma = Marshmallow(app1)
manager = LoginManager(app1)

load_dotenv("ft.env")


def feature_toggle(ft_name):
    def decorator(func):
        def wrapper(*args, **kwargs):
            # Получаем значение флага из переменных окружения
            flag_enabled = os.getenv(ft_name, 'False').lower() == 'true'

            if flag_enabled:
                return func(*args, **kwargs)
            else:
                return dict(message='NOT_FOUND'), 404
        return wrapper
    return decorator


from user_api import models, routs
