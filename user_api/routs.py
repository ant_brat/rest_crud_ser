from user_api.models import Bank_user, user_schema, users_schema
#from acc_ser_crud import app2, db, feature_toggle
from user_api import app1, db, feature_toggle
from flask import Flask, request, redirect, url_for
from flask_login import login_user, LoginManager, UserMixin, login_required, logout_user


@app1.route("/user", methods=['GET'], endpoint='FT_1')
@login_required
@feature_toggle('FT_GET_USER_IS_ENABLE')
def users_page():
    all_users = Bank_user.query.all()
    result = users_schema.dump(all_users)
    return result


@app1.route("/user/<user_id>", methods=['GET'], endpoint='FT_2')
@login_required
@feature_toggle('FT_GET_SINGLE_USER_IS_ENABLE')
def single_user(user_id):
    user = db.session.get(Bank_user, user_id)
    return user_schema.jsonify(user)


@app1.route("/user/add", methods=['POST'], endpoint='FT_3')
@login_required
@feature_toggle('FT_POST_USER_IS_ENABLE')
def add_user(user_id=None):
    user_fio = request.json['user_fio']
    user_email = request.json['user_email']
    user_ac_name = request.json['user_ac_name']
    user_pass = request.json['user_pass']

    new_user = Bank_user(user_id, user_fio, user_email, user_ac_name, user_pass)

    db.session.add(new_user)
    db.session.commit()

    return user_schema.jsonify(new_user)


@app1.route("/user/update/<user_id>", methods=['PUT'], endpoint='FT_4')
@login_required
@feature_toggle('FT_PUT_USER_IS_ENABLE')
def update_user(user_id):
    user = db.session.get(Bank_user, user_id)

    user_fio = request.json['user_fio']
    user_email = request.json['user_email']
    user_ac_name = request.json['user_ac_name']
    user_pass = request.json['user_pass']

    user.user_fio = user_fio
    user.user_email = user_email
    user.user_ac_name = user_ac_name
    user.user_pass = user_pass

    db.session.commit()

    return user_schema.jsonify(user)


@app1.route("/user/delete/<user_id>", methods=['DELETE'], endpoint='FT_5')
@login_required
@feature_toggle('FT_DELETE_USER_IS_ENABLE')
def delete_user(user_id):
    user = db.session.get(Bank_user, user_id)
    db.session.delete(user)
    db.session.commit()

    return user_schema.jsonify(user)


