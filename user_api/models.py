from user_api import db, ma, manager
from flask_login import UserMixin


class UserSchema(ma.Schema):
    class Meta:
        fields = ('user_id', 'user_fio', 'user_email', 'user_ac_name', 'user_pass')


user_schema = UserSchema()
users_schema = UserSchema(many=True)


class Bank_user(db.Model, UserMixin):
    user_id = db.Column(db.Integer, primary_key=True)
    user_fio = db.Column(db.String(32), nullable=False)
    user_email = db.Column(db.String(32), nullable=False)
    user_ac_name = db.Column(db.String(32), nullable=False)
    user_pass = db.Column(db.String, nullable=False)

    def __init__(self, user_id, user_fio, user_email, user_ac_name, user_pass):
        self.user_id = user_id
        self.user_fio = user_fio.strip()
        self.user_email = user_email.strip()
        self.user_ac_name = user_ac_name.strip()
        self.user_pass = user_pass.strip()

    def get_id(self):
        return self.user_id


@manager.user_loader
def load_user(user_id):
    return Bank_user.query.get(user_id)
