from user_api.models import Bank_user, user_schema, users_schema
from user_api.routs import users_page, single_user, update_user, add_user, delete_user
from unittest.mock import patch, MagicMock
import unittest
from flask import request, jsonify
from user_api import app1


class Test_user_ser_crud(unittest.TestCase):

    def setUp(self):
        app1.config['TESTING'] = True
        app1.config['LOGIN_DISABLED'] = True

    @patch('user_api.routs.Bank_user')
    def test_users_page(self, mock_query):
        with app1.test_request_context(content_type='application/json'):

            mock_query.query.all.return_value = [Bank_user(1, 'Anan', 'tikur@ana.ru', 'potnyara', 'asdfa'),
                                                 Bank_user(2, 'Dvor', 'user2@ert,ru', 'ustal', '324fs')]

            self.assertEqual(users_page(), [{"user_id": 1, "user_fio": "Anan", "user_email": 'tikur@ana.ru', "user_ac_name": 'potnyara', "user_pass": 'asdfa'},
                                            {"user_id": 2, "user_fio": "Dvor", "user_email": 'user2@ert,ru', "user_ac_name": 'ustal', "user_pass": '324fs'}])

    @patch('user_api.routs.db')
    def test_single_user(self, db_query):
        with app1.test_request_context(content_type='application/json'):

            test_user = Bank_user(1, 'Anan', 'tikur@ana.ru', 'potnyara', 'asdfa')
            db_query.session.get.return_value = test_user

            self.assertEqual(single_user(1).get_json(), user_schema.jsonify(test_user).get_json())

    @patch('user_api.routs.db')
    def test_add_user(self, db_query):
        with app1.test_request_context(content_type='application/json', json={
            'user_fio': 'Pekar',
            'user_email': 'Gena@pek.ru',
            'user_ac_name': 'zhalo',
            'user_pass': '666'
        }):
            m_user_fio = request.json['user_fio']
            self.assertEqual(m_user_fio, 'Pekar')

            m_user_email = request.json['user_email']
            self.assertEqual(m_user_email, 'Gena@pek.ru')

            m_user_ac_name = request.json['user_ac_name']
            self.assertEqual(m_user_ac_name, 'zhalo')

            m_user_pass = request.json['user_pass']
            self.assertEqual(m_user_pass, '666')

            mock_user = Bank_user(1, m_user_fio, m_user_email, m_user_ac_name, m_user_pass)

            self.assertEqual(add_user(1).get_json(), user_schema.jsonify(mock_user).get_json())

    @patch('user_api.routs.db')
    def test_update_user(self, db_query):
        test_acc = Bank_user(1, 'Anan', 'tikur@ana.ru', 'potnyara', 'asdfa')
        db_query.session.get.return_value = test_acc

        with app1.test_request_context(content_type='application/json', json={
            'user_fio': 'Pekar',
            'user_email': 'Gena@pek.ru',
            'user_ac_name': 'zhalo',
            'user_pass': '666'
        }):
            m_user_fio = request.json['user_fio']
            self.assertEqual(m_user_fio, 'Pekar')

            m_user_email = request.json['user_email']
            self.assertEqual(m_user_email, 'Gena@pek.ru')

            m_user_ac_name = request.json['user_ac_name']
            self.assertEqual(m_user_ac_name, 'zhalo')

            m_user_pass = request.json['user_pass']
            self.assertEqual(m_user_pass, '666')

            mock_user = Bank_user(1, m_user_fio, m_user_email, m_user_ac_name, m_user_pass)

            self.assertEqual(update_user(1).get_json(), user_schema.jsonify(mock_user).get_json())

    @patch('user_api.routs.db')
    def test_delete_user(self, db_query):
        with app1.test_request_context(content_type='application/json'):
            test_user = Bank_user(1, 'Anan', 'tikur@ana.ru', 'potnyara', 'asdfa')
            db_query.session.get.return_value = test_user

            self.assertEqual(delete_user(1).get_json(), user_schema.jsonify(test_user).get_json())

#    @patch('acc_ser_crud.request')
#    def test_add_acc(self, db_request):
#        request_data = {
#            'user_login': 'test_user',
#            'acc_num': '1234567890',
#            'acc_cur': 'USD',
#            'acc_sum': 1000
#        }
#
##            json_mock = MagicMock(return_value={"acc_id": 1, "acc_cur": "USD", "acc_num": 12345678900987654321, "acc_sum": 1000, "user_login": "tikur"})
#        db_request.json.return_value = request_data
#        userlogin_mock = MagicMock(return_value='tikur', name='db_request.json["user_login"]')
#        request_data['user_login'] = userlogin_mock
##        accnum_mock = MagicMock(return_value=12345678900987654321)
##        db_request.request.json['acc_num'].return_value = accnum_mock
##        acccur_mock = MagicMock(return_value='USD')
##        db_request.request.json['acc_cur'].return_value = acccur_mock
##        accsum_mock = MagicMock(return_value='1000')
##        db_request.request.json['acc_sum'].return_value = accsum_mock
#
#        new_acc = Bank_acc(None, "tikur", 12345678900987654321, 'USD', 1000)
#        print(new_acc)
#
##        db_request.session.add(new_acc).return_value = [Bank_acc(1, 'tikur', 12345678900987654321, 'USD', 1000)]
#
#        mock_jsonify = MagicMock(return_value={"acc_id": 1, "acc_cur": "USD", "acc_num": 12345678900987654321, "acc_sum": 1000, "user_login": "tikur"},
#                                 name='account_schema')
#        account_schema.jsonify = mock_jsonify
#
#        self.assertEqual(add_acc(1), {"acc_id": 1, "acc_cur": "USD", "acc_num": 12345678900987654321, "acc_sum": 1000, "user_login": "tikur"})
#