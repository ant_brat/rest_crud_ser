import os
#from models import Bank_acc, Bank_user, load_user
from dotenv import load_dotenv
from flask import Flask, request, redirect, url_for
from flask_login import login_user, LoginManager, UserMixin, login_required, logout_user
#from werkzeug.security import check_password_hash, generate_password_hash
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow


app2 = Flask(__name__)
app2.secret_key = 'krovostok'
app2.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:Lfirbyp1@db:5432/GPB_test'
app2.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app2)
ma = Marshmallow(app2)
manager = LoginManager(app2)


load_dotenv("ft.env")


def feature_toggle(ft_name):
    def decorator(func):
        def wrapper(*args, **kwargs):
            # Получаем значение флага из переменных окружения
            flag_enabled = os.getenv(ft_name, 'False').lower() == 'true'

            if flag_enabled:
                return func(*args, **kwargs)
            else:
                print(f"Тогл '{ft_name}' выключен для данной секции")
                return dict(message=f"Тогл '{ft_name}' выключен для данной секции"), 404
        return wrapper
    return decorator


from acc_api import models, routs
