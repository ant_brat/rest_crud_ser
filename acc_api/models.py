from acc_api import db, ma, manager
from flask_login import UserMixin


class AccSchema(ma.Schema):
    class Meta:
        fields = ('acc_id', 'user_login', 'acc_num', 'acc_cur', 'acc_sum')


account_schema = AccSchema()
accounts_schema = AccSchema(many=True)


class Bank_user(db.Model, UserMixin):
    user_id = db.Column(db.Integer, primary_key=True)
    user_fio = db.Column(db.String(32), nullable=False)
    user_email = db.Column(db.String(32), nullable=False)
    user_ac_name = db.Column(db.String(32), nullable=False)
    user_pass = db.Column(db.String, nullable=False)

    def __init__(self, user_id, user_fio, user_email, user_ac_name, user_pass):
        self.user_id = user_id
        self.user_fio = user_fio.strip()
        self.user_email = user_email.strip()
        self.user_ac_name = user_ac_name.strip()
        self.user_pass = user_pass.strip()

    def get_id(self):
        return self.user_id


class Bank_acc(db.Model):
    acc_id = db.Column(db.Integer, primary_key=True)
    user_login = db.Column(db.String(100), nullable=True)
    acc_num = db.Column(db.Integer, nullable=False)
    acc_cur = db.Column(db.String(5), nullable=False)
    acc_sum = db.Column(db.Integer, nullable=False)

    def __init__(self, acc_id, user_login, acc_num, acc_cur, acc_sum):
        self.acc_id = acc_id
        self.user_login = user_login.strip()
        self.acc_num = acc_num
        self.acc_cur = acc_cur.strip()
        self.acc_sum = acc_sum

    def get_id(self):
        return self.acc_id


@manager.user_loader
def load_user(user_id):
    return Bank_user.query.get(user_id)
