from acc_api.models import Bank_acc, Bank_user, accounts_schema, account_schema
#from acc_ser_crud import app2, db, feature_toggle
from acc_api import app2, db, feature_toggle
from flask import Flask, request, redirect, url_for
from flask_login import login_user, LoginManager, UserMixin, login_required, logout_user


#@app2.route("/", methods=['GET'])
#def start_page():
#    all_acc = Bank_acc.query.all()
#    result = accounts_schema.dump(all_acc)
#    return jsonify(result)

@app2.route("/", methods=['GET', 'POST'])
def login_page():

    if request.method == 'POST':
        login = request.json['username']
        password = request.json['pwd']
        if login and password:
            user = Bank_user.query.filter_by(user_ac_name=login).first()

            if user and user.user_pass == password:
                print(user)
                login_user(user)
                return 'Login Succeded'
            else:
                return 'Login or Password is incorrect'
        else:
            return 'Please fill login and password fields'
    else:
#    img_path = url_for("static", filename="images/image.png")
        return 'Login NeSucceded'


@app2.route("/logout", methods=['GET', 'POST'])
@login_required
def logout():
    logout_user()
    return redirect(url_for('login_page'))


@app2.route("/account", methods=['GET'], endpoint='FT_1')
@login_required
@feature_toggle('FT_GET_ACC_IS_ENABLE')
def accounts_page():
    all_acc = Bank_acc.query.all()
    result = accounts_schema.dump(all_acc)
    return result


@app2.route("/account/<acc_id>", methods=['GET'], endpoint='FT_2')
@login_required
@feature_toggle('FT_GET_SINGLE_ACC_IS_ENABLE')
def single_acc(acc_id):
    account = db.session.get(Bank_acc, acc_id)
    return account_schema.jsonify(account)


@app2.route("/account/add", methods=['POST'], endpoint='FT_3')
@login_required
@feature_toggle('FT_POST_ACC_IS_ENABLE')
def add_acc(acc_id=None):
    user_login = request.json['user_login']
    acc_num = request.json['acc_num']
    acc_cur = request.json['acc_cur']
    acc_sum = request.json['acc_sum']

    new_acc = Bank_acc(acc_id, user_login, acc_num, acc_cur, acc_sum)

    db.session.add(new_acc)
    db.session.commit()

    return account_schema.jsonify(new_acc)


@app2.route("/account/update/<acc_id>", methods=['PUT'], endpoint='FT_4')
@login_required
@feature_toggle('FT_PUT_ACC_IS_ENABLE')
def update_acc(acc_id):
    account = db.session.get(Bank_acc, acc_id)

    user_login = request.json['user_login']
    acc_num = request.json['acc_num']
    acc_cur = request.json['acc_cur']
    acc_sum = request.json['acc_sum']

    account.user_login = user_login
    account.acc_num = acc_num
    account.acc_cur = acc_cur
    account.acc_sum = acc_sum

    db.session.commit()

    return account_schema.jsonify(account)


@app2.route("/account/delete/<acc_id>", methods=['DELETE'], endpoint='FT_5')
@login_required
@feature_toggle('FT_DELETE_ACC_IS_ENABLE')
def delete_acc(acc_id):
    account = db.session.get(Bank_acc, acc_id)
    db.session.delete(account)
    db.session.commit()

    return account_schema.jsonify(account)
