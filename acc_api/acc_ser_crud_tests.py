from acc_api.models import Bank_acc, account_schema
from acc_api.routs import accounts_page, single_acc, add_acc, update_acc, delete_acc
from unittest.mock import patch
import unittest
from flask import request
from acc_api import app2


class Test_acc_ser_crud(unittest.TestCase):
    def setUp(self):
        app2.config['TESTING'] = True
        app2.config['LOGIN_DISABLED'] = True

    @patch('acc_api.routs.Bank_acc')
    def test_accounts_page(self, mock_query):
        with app2.test_request_context(content_type='application/json'):
            mock_query.query.all.return_value = [Bank_acc(1, 'tikur', 12345678900987654321, 'USD', 1000),
                                                 Bank_acc(2, 'user2', 12345678900987654322, 'EUR', 2000)]
            self.assertEqual(accounts_page(), [{"acc_id": 1, "acc_cur": "USD", "acc_num": 12345678900987654321, "acc_sum": 1000, "user_login": "tikur"},
                                               {"acc_id": 2, "acc_cur": "EUR", "acc_num": 12345678900987654322, "acc_sum": 2000, "user_login": "user2"}])

    @patch('acc_api.routs.db')
    def test_single_acc(self, db_query):
        with app2.test_request_context(content_type='application/json'):
            test_acc = Bank_acc(1, 'test_user', 12345678900987654321, 'USD', 1000)
            db_query.session.get.return_value = test_acc
#        mock_jsonify = MagicMock(return_value={"acc_id": 1, "acc_cur": "USD", "acc_num": 12345678900987654321, "acc_sum": 1000, "user_login": "tikur"},
#                                 name='account_schema')
#       account_schema.jsonify = mock_jsonify
            self.assertEqual(single_acc(1).get_json(), account_schema.jsonify(test_acc).get_json())

    @patch('acc_api.routs.db')
    def test_add_acc(self, db_query):
        with app2.test_request_context(content_type='application/json', json={
                'user_login': 'test_user',
                'acc_num': 12345678900987654321,
                'acc_cur': 'USD',
                'acc_sum': 1000
        }):
            m_user_login = request.json['user_login']
            self.assertEqual(m_user_login, 'test_user')

            m_acc_num = request.json['acc_num']
            self.assertEqual(m_acc_num, 12345678900987654321)

            m_acc_cur = request.json['acc_cur']
            self.assertEqual(m_acc_cur, 'USD')

            m_acc_sum = request.json['acc_sum']
            self.assertEqual(m_acc_sum, 1000)

            mock_acc = Bank_acc(1, m_user_login, m_acc_num, m_acc_cur, m_acc_sum)

            self.assertEqual(add_acc(1).get_json(), account_schema.jsonify(mock_acc).get_json())

    @patch('acc_api.routs.db')
    def test_update_acc(self, db_query):
        test_acc = Bank_acc(1, 'test_user', 12345678900987654321, 'USD', 1002)
        db_query.session.get.return_value = test_acc

        with app2.test_request_context(content_type='application/json', json={
                'user_login': 'test_user_2',
                'acc_num': 12345678900987654329,
                'acc_cur': 'RUB',
                'acc_sum': 10005
        }):
            m_user_login = request.json['user_login']
            self.assertEqual(m_user_login, 'test_user_2')

            m_acc_num = request.json['acc_num']
            self.assertEqual(m_acc_num, 12345678900987654329)

            m_acc_cur = request.json['acc_cur']
            self.assertEqual(m_acc_cur, 'RUB')

            m_acc_sum = request.json['acc_sum']
            self.assertEqual(m_acc_sum, 10005)

            mock_acc = Bank_acc(1, m_user_login, m_acc_num, m_acc_cur, m_acc_sum)

            self.assertEqual(update_acc(1).get_json(), account_schema.jsonify(mock_acc).get_json())

    @patch('acc_api.routs.db')
    def test_delete_acc(self, db_query):
        with app2.test_request_context(content_type='application/json'):
            test_acc = Bank_acc(1, 'test_user', 12345678900987654321, 'USD', 1000)
            db_query.session.get.return_value = test_acc

            self.assertEqual(delete_acc(1).get_json(), account_schema.jsonify(test_acc).get_json())
